---
title: Gitlab CI + Docker
subtitle: Ejemplo de implementacion con Gitlab CI + Docker.
date: 2017-07-05
tags: ["gitlab", "CI","YAML"]
---

Al realizar una implementacion de Gitlab CI podemos usar docker. Los pros y contras son los siguientes 

# Pros

* Al usar docker se eliminan problemas de "corre en mi computadora", los cuales son frustrantes y hacen perder mucho tiempo
* Gitlab y Digital ocean estan unidos por los que tienes 2000 minutos de CI al mes por equipo. [Link]("https://about.gitlab.com/2016/04/19/gitlab-partners-with-digitalocean-to-make-continuous-integration-faster-safer-and-more-affordable/">)
* Dar de alta runners es muy facil, si llegas a quedarte sin minutos de CI puedes poner una computadora cualquiera a correrlo
* Puedes configurar tu configuracion de CI con el archivo gitlab-ci-yml. [Documentacion](https://docs.gitlab.com/ee/ci/yaml/#gitlab-ci-yml)
* Puedes almacenar las imagenes de docker en sus container registries. Que son como un repositorio de imagenes de docker, parecido a docker hub. Lo que facilita el deploy a otras plataformas. [Documentacion](https://gitlab.com/help/user/project/container_registry)

# Contras

* Usar docker vuelve cada pipeline muy pesado, ya que se tiene que generar una imagen y cargarla cada vez que se corre
* Relativo al punto anterior, es muy facil comerse los minutos que se tienen de CI al mes
* Algunas cosas de la configuracion de CI no estan muy bien en mi opinion, como el manejo de artefactos
* La documentacion es poco amigable, le hacen falta ejemplos practicos


# .gitlab-ci.yml
Este archivo lo tenemos que agregar al root de nuestro proyecto y es donde definimos la configuracion completa de nuestro CI.  
Primero veamos la implementacion inicial que tengo y vamos parte por parte.

```
image: docker:git

services:
- docker:dind

stages:
  - build

before_script:
  - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY

variables:
  CONTAINER_TEST_IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_NAME:$CI_COMMIT_SHA

build:
  stage: build

  script: 
    - docker build -t "$CONTAINER_TEST_IMAGE" .
    - docker push "$CONTAINER_TEST_IMAGE"
```

## Analisis

### Image y services

```
image: docker:git
```

Con la linea de image que la imagen de docker que queremos que usar en docker, al poner ":git" decimos que queremos usar la version con el tag de git, que es una version de docker que ya tiene git instalado, el link de docker hub es el siguiente. [Link](https://hub.docker.com/r/library/docker/tags/)

Cabe mencionar que al seleccionar una imagen, el CI correra sobre esa imagen. [Documentacion](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html)

```
services:
- docker:dind
```

Con la linea de services podemos definir cuales son las otras imagenes de docker que queremos, el proceso es complicado, pero podemos imaginarlo como un contenedor "Hermano" al que nuestra imagen principal (la definida en image) tiene acceso. [Documentacion Docker](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#what-is-a-service)

En este caso queremos tener docker instalado dentro de la imagen docker que corremos, a esto se le llama docker in docker o "dind", no es una practica muy recomendada como podemos ver en [Link](https://jpetazzo.github.io/2015/09/03/do-not-use-docker-in-docker-for-ci/) sin embargo no me he metido a picarle, asi que esta solucion me es suficiente por el momento.



### Stages
```
stages:
  - build
```
Con stages podemos definir cuales son los pasos que queremos que se corra en una sesion de CI, por ejemplo si quisieramos que cada CI tenga, build, test y deploy, aqui es donde definimos que el CI va a tener esas 3 etapas. En este caso, todavia no implementamos tests ni hemos hecho deploy por lo que nos vamos con build, puedes no definirlo y creo que te pone esas 3 etapas, viene mas a detalle en la documentacion.[Link](https://docs.gitlab.com/ee/ci/yaml/#stages)

NOTA: El orden en el que pongamos los stages va a ser el orden en el que se ejecutan.

### before_script
```
before_script:
  - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
```
El comando de before_script nos permite definir cuales son los comandos que queremos que se ejecuten antes de que comience el CI (En este caso es global porque esta en el primer nivel de indentacion, entonces en todos los trabajos se va a correr el comando).

En este caso lo que queremos hacer es logear al container registry de gitlab, usamos el usuario gitlab-ci-token, ya que no vamos hardcodear nuestro usuario y contraseña por razones obvias. De contraseña usamos de $CI_BUILD_TOKEN para que mande la token y finalmente $CI_REGISTRY contiene la direccion del container registry.

### variables
```
variables:
  CONTAINER_TEST_IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_NAME:$CI_COMMIT_SHA1
```

Variables nos permite meter variables globales, en este caso vamos a guardar cual es la direccion en la que queremos que se guarde la imagen de docker que vamos a generar, el resultado de esto seria algo como esto:  
```registry.gitlab.com/{usuario}/{proyecto}/{branch}:{ID del commit}```

### build
```
build:
```
Este es el nombre de un trabajo (job), en este caso le puse build por facilidad, pero aqui se le puede poner el nombre que sea, es meramente referencia. 

Todo lo que pongamos adentro de este trabajo va a ser lo que se va a ejecutar para este trabajo.

### stage
```
  stage: build
```
Con esta linea estamos asociando el trbaajo de build al stage de build.

### script
```
script: 
    - docker build -t "$CONTAINER_TEST_IMAGE" .
    - docker push "$CONTAINER_TEST_IMAGE"
```

Despues de toda la preparacion, ahora si, vamos a ejecutar el trabajo de build. 

Recapitulando; estamos en el job llamado "build" que pertenece al stage de "build", sobre una imagen de docker, que tiene dind como servicio y en la cual ya nos encontramos logeados al container registry de nuestro proyecto y donde tenemos una variable global que define el nombre y direccion del contenedor que vamos a usar para la etapa de build.

NOTA: Gitlab se encarga de copiar el repositorio que tenemos a ```/builds/$CI_PROJECT_PATH/``` (algo como ```/builds/{usuario}/{proyecto}/```) e inicia la imagen de docker en ese directorio (Como is agregara WORKPATH en el dockerfile)

Lo que hacemos sobre este script es sencillo, vamos a poner ```docker build``` para crear una imagen de docker con ```-t``` para asignarle como tag el nombre de la imagen que definimos dentro de nuestras variables globales, con el punto, le estamos diciendo que queremos que corra sobre el directorio actual, por lo que va a buscar un archivo llamado dockerfile para construir la imagen, si quisieramos que tome otro archivo ponemos la opcion de ```-f {nombre del archivo}```.

 De ahi hacemos un ```docker push``` lo cual va a cargar la imagen al container registry del proyecto.

## Lanzar el trabajo

 Para poder lanzar un job simplemente hay que subir nuestra configuracion al repositorio (el archivo .gitlab-ci.yml) y el pipeline (el que ejecuta el trabajo) se lanzara automaticamente.